# fortress
Fortress is an Arch Linux configuration for running many blockchains on a single, fast bare metal server with unlimited bandwidth.  Server costs for a Fortress are paid by selling distributed VPN services on the [Tachyon Network](https://tachyon.eco).  

## Development Method
The first task for building the first version of Fortress was just getting everything running.  After that was done, it was necessary to switch to another server, because I'd bought a server wtih a not-so-great network card.  Using [hetzner.de](https://hetzner.de), I settled on their server SB47 for my second attempt, with a spec like:

Intel Xeon E3-1245V2
3x HDD SATA 3,0 TB Enterprise
4x RAM 8192 MB DDR3 ECC
Intel 82574L NIC
LSI MegaRAID SAS 9260-4i

## Blockchains
* Bitcoin
* V Systems
* Cosmos
* Tezos
* Ethereum
* Peercoin
* Primecoin
* Whaleshares
* Litecoin
* Cardano
* Algorand
* Incognito


## Money Makers
* Tachyon VPN
* Throttled Monero Mining
....hopefully others coming soon, too

## Bandwidth Tracking
* vnstat
